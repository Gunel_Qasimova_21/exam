import java.util.ArrayList;
public class Exam {


	public static void main(String[] args) {
	Animal animalclass=new Dog();
	Animal animalclass1=new Fox();
	Animal animalclass2=new Lion();
    Animal animalclass3=new Wolf();
    animalclass.LanguageID="0";
    animalclass1.LanguageID="1";
    animalclass2.LanguageID="2";
    animalclass3.LanguageID="3";
    animalclass.LanguageName="a symbol of loyality";
    animalclass1.LanguageName="a symbol of trickery";
    animalclass2.LanguageName="a symbol of leadership";
    animalclass3.LanguageName="a symbol of courage";
    ArrayList <Animal> list = new ArrayList<Animal>();
    list.add(animalclass);
    list.add(animalclass1);
    list.add(animalclass2);
    list.add(animalclass3);
    for (int i=0; i<list.size(); i++){
    	list.get(i).greet();
    	list.get(i).saygoodbye();
    	System.out.println(list.get(i).LanguageName);
    	System.out.println(list.get(i).LanguageID);
    }
	}

}
